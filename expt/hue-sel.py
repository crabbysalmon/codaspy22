#!/usr/bin/env python3

import sys
sys.path.insert(1, '../lib')
import argparse
import ast
from datetime import datetime
from geopy.distance import distance
import lmdk_bgt
import lmdk_lib
import lmdk_sel
import exp_mech
import math
import numpy as np
from matplotlib import pyplot as plt
import time


def main(args):
  # User's consumption
  seq = lmdk_lib.load_data(args, 'cons')
  # The name of the dataset
  d = 'HUE'
  # The landmarks percentages
  lmdks_pct = [0, 20, 40, 60, 80, 100]
  # Landmarks' thresholds
  lmdks_th = [0, .54, .68, .88, 1.12, 10]
  # The privacy budget
  epsilon = 1.0

  # Number of methods
  n = 3
  # Width of bars
  bar_width = 1/(n + 1)
  # The x axis
  x_i = np.arange(len(lmdks_pct))
  x_margin = bar_width*(n/2 + 1)

  print('\n##############################', d, '\n')

  # Initialize plot
  lmdk_lib.plot_init()
  # The x axis
  plt.xticks(x_i, np.array(lmdks_pct, int))
  plt.xlabel('Landmarks (%)')  # Set x axis label.
  plt.xlim(x_i.min() - x_margin, x_i.max() + x_margin)
  # The y axis
  plt.ylabel('Mean absolute error (kWh)')  # Set y axis label.
  plt.yscale('log')
  plt.ylim(.1, 100000)
  # Bar offset
  x_offset = -(bar_width/2)*(n - 1)

  mae_u = np.zeros(len(lmdks_pct))
  mae_u_sel= np.zeros(len(lmdks_pct))
  mae_s = np.zeros(len(lmdks_pct))
  mae_s_sel = np.zeros(len(lmdks_pct))
  mae_a = np.zeros(len(lmdks_pct))
  mae_a_sel = np.zeros(len(lmdks_pct))
  mae_evt = 0
  mae_usr = 0

  for i, pct in enumerate(lmdks_pct):
    # Find landmarks
    lmdks = seq[seq[:, 1] < lmdks_th[i]]

    for _ in range(args.iter):

      lmdks_sel, eps_out = lmdk_sel.find_lmdks(seq, lmdks, epsilon)

      # Skip
      rls_data_s, _ = lmdk_bgt.skip_cons(seq, lmdks, eps_out)
      mae_s[i] += lmdk_bgt.mae_cons(seq, rls_data_s)/args.iter
      rls_data_s_sel, _ = lmdk_bgt.skip_cons(seq, lmdks_sel, eps_out)
      mae_s_sel[i] += lmdk_bgt.mae_cons(seq, rls_data_s_sel)/args.iter

      # Uniform
      rls_data_u, _ = lmdk_bgt.uniform_cons(seq, lmdks, eps_out)
      mae_u[i] += lmdk_bgt.mae_cons(seq, rls_data_u)/args.iter
      rls_data_u_sel, _ = lmdk_bgt.uniform_cons(seq, lmdks_sel, eps_out)
      mae_u_sel[i] += lmdk_bgt.mae_cons(seq, rls_data_u_sel)/args.iter

      # Adaptive
      rls_data_a, _, _ = lmdk_bgt.adaptive_cons(seq, lmdks, eps_out, .5, .5)
      mae_a[i] += lmdk_bgt.mae_cons(seq, rls_data_a)/args.iter
      rls_data_a_sel, _, _ = lmdk_bgt.adaptive_cons(seq, lmdks_sel, eps_out, .5, .5)
      mae_a_sel[i] += lmdk_bgt.mae_cons(seq, rls_data_a_sel)/args.iter

      # Calculate once
      if pct == lmdks_pct[0]:
        # Event
        rls_data_evt, _ = lmdk_bgt.uniform_cons(seq, lmdks, epsilon)
        mae_evt += lmdk_bgt.mae_cons(seq, rls_data_evt)/args.iter
      elif pct == lmdks_pct[-1]:
        # User
        rls_data_usr, _ = lmdk_bgt.uniform_cons(seq, lmdks, epsilon)
        mae_usr += lmdk_bgt.mae_cons(seq, rls_data_usr)/args.iter

  plt.axhline(
    y = mae_evt,
    color = '#212121',
    linewidth=lmdk_lib.line_width
  )
  plt.text(x_i[-1] + x_i[-1]*.14, mae_evt - mae_evt*.14, 'event')

  plt.axhline(
    y = mae_usr,
    color = '#616161',
    linewidth=lmdk_lib.line_width
  )
  plt.text(x_i[-1] + x_i[-1]*.14, mae_usr - mae_usr*.14, 'user')

  # Plot bars
  plt.bar(
    x_i + x_offset,
    mae_s_sel,
    bar_width,
    label='Skip',
    linewidth=lmdk_lib.line_width
  )
  plt.plot(
    x_i + x_offset,
    mae_s,
    marker='_',
    markersize=lmdk_lib.marker_size + lmdk_lib.line_width,
    markeredgewidth=lmdk_lib.line_width,
    markeredgecolor='#bdbdbd',
    linestyle='none',
  )
  x_offset += bar_width
  plt.bar(
    x_i + x_offset,
    mae_u_sel,
    bar_width,
    label='Uniform',
    linewidth=lmdk_lib.line_width
  )
  plt.plot(
    x_i + x_offset,
    mae_u,
    marker='_',
    markersize=lmdk_lib.marker_size + lmdk_lib.line_width,
    markeredgewidth=lmdk_lib.line_width,
    markeredgecolor='#bdbdbd',
    linestyle='none',
  )
  x_offset += bar_width
  plt.bar(
    x_i + x_offset,
    mae_a_sel,
    bar_width,
    label='Adaptive',
    linewidth=lmdk_lib.line_width
  )
  plt.plot(
    x_i + x_offset,
    mae_a,
    marker='_',
    markersize=lmdk_lib.marker_size + lmdk_lib.line_width,
    markeredgewidth=lmdk_lib.line_width,
    markeredgecolor='#bdbdbd',
    linestyle='none',
  )

  path = str('../../rslt/bgt_cmp/' + d)
  # Plot legend
  lmdk_lib.plot_legend()
  # Show plot
  # plt.show()
  # Save plot
  lmdk_lib.save_plot(path + '-sel-cmp.pdf')
  print('[OK]', flush=True)


def parse_args():
  '''
    Parse arguments.

    Optional:
      res  - The results archive file.
      iter - The total iterations.
  '''
  # Create argument parser.
  parser = argparse.ArgumentParser()

  # Mandatory arguments.

  # Optional arguments.
  parser.add_argument('-r', '--res', help='The results archive file.', type=str, default='/home/***/Cloud/Data/HUE/Results.zip')
  parser.add_argument('-i', '--iter', help='The total iterations.', type=int, default=1)

  # Parse arguments.
  args = parser.parse_args()

  return args


if __name__ == '__main__':
  try:
    start_time = time.time()
    main(parse_args())
    end_time = time.time()
    print('##############################')
    print('Time elapsed: %s' % (time.strftime('%H:%M:%S', time.gmtime(end_time - start_time))))
    print('##############################')
  except KeyboardInterrupt:
    print('Interrupted by user.')
    exit()
